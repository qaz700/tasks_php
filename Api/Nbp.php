<?php

namespace Api;

class Nbp {

    public function getCurrencyRate($code) {
        $rate = null;

        $client = \Symfony\Component\HttpClient\HttpClient::create();
        $url = "http://api.nbp.pl/api/exchangerates/rates/a/" .
                $code
                . "/today/?format=json";
        $response = $client->request("GET", $url);
        $status = $response->getStatusCode();

        if ($status == 200) {
            $data = json_decode($response->getContent(), true);
            $rate = $data['rates']['0']['mid'];
        }

        return $rate;
    }

}
