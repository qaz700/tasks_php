<?php

namespace Api;

use Api\ResponseInterface;

/**
 * Description of MyResponse
 *
 * @author qaz
 */
class MyResponse implements ResponseInterface 
{

    protected string $returnResult;

    public function __construct($returnResult, $success) 
    {
        $this->returnResult = $returnResult;
        $this->success = $success;
    }

    public function getContent(bool $jsonEncoded = true) 
    {
        return json_encode(
                array(
                    'returnResult' => number_format($this->returnResult, 5)
                )
        );
    }

    public function getHttpCode(): int 
    {
        if ($this->success) {
            return 200;
        } else {
            return 500;
        }
    }

}