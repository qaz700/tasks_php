<?php
declare(strict_types=1);

namespace Api\ExchangeRates;

use Api\ResponseInterface;
use Api\Nbp;
use \Api\MyResponse;

class CalculatorController
{

    /**
     * @param array $inputArray assume any input array (like $_GET)
     * @return ResponseInterface
     */
    public function calculateCurrentExchangeRateAction(array $inputArray = []): ResponseInterface
    {
        $resulting = 0;
        $success = false;
        $nbp = new Nbp();
        $rateFrom = $nbp->getCurrencyRate($inputArray['fromCurrency']);
        $rateTo = $nbp->getCurrencyRate($inputArray['toCurrency']);

        if ($rateFrom != null && $rateTo != null) {
            $resulting = $rateFrom / $rateTo;

            if (isset($inputArray['amount'])) {
                $resulting = $resulting * $inputArray['amount'];
            }
            $success = true;
        }

        return new MyResponse($resulting, $success);
    }

    /**
     * @param array $inputArray assume any input array (like $_GET)
     * @return ResponseInterface
     */
    public function getRateStatsAction(array $inputArray = []): ResponseInterface
    {

    }

}