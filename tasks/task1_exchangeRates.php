<?php

require_once 'bootstrap.php';

/**
 * ====== PLEASE PROVIDE THE FOLLOWING: =======
 * Development Time Estimate [in hours]:    8
 * Actual Development Time [in hours]:      ~3
 * Your thoughts and comments: Fajnie że jest zadanie udostępnione jako projekt.
 * Troche gorzej że na Debianie trzeba chwile powalczyć żeby na czystej dystrybucji 
 * wszystko zadzaiałało ale i tak fajne zadanie rekrutacyjne.
 * Jeszcze można by się szarpnąć na dorobienie obsługi błędów 
 * (brak odpowiedzi nbp czy też źle podany kod waluty) ale wysyłam tak jak jest.
 * Info o konieczności posiadania profilu na bitbucket.org rozjaśniło by sprawę forka ;) 
 * ============================================
 */

/**
 * correct input EXAMPLE - it may be modified (and will be modified during task assessment)
 */
$_GET = [
    'fromCurrency' => 'USD',
    'toCurrency' => 'GBP',
    'amount' => 50,
];

$controller = new \Api\ExchangeRates\CalculatorController();
$response = $controller->calculateCurrentExchangeRateAction($_GET);


// print the returned response
http_response_code($response->getHttpCode());
header('Content-type: application/json');
echo $response->getContent(true);
